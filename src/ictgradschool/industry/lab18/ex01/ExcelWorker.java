package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ExcelWorker {
    private String fileSurname = "Surnames.txt";
    private String fileFirstname = "FirstNames.txt";
    private String fileOut = "Data_Out1.txt";
    private final int CLASS_SIZE = 550;
    private final int NUM_LABS = 3;
    private List<String> firstNameList;
    private List<String> surnameList;

    private final int[] LAB_BOUNDS = {15, 25, 65};
    private final int[] TEST_BOUNDS = {20, 65, 90};
    private final int[] EXAM_BOUNDS = {20, 60, 90};


    public String generateScore(int randStudentSkill, int[] bounds, boolean exam) {
        String ret = "";
        if(exam && randStudentSkill <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb > 5) {
                ret += (int) (Math.random() * 40); //[0,39]
            }

        } else if (!exam && randStudentSkill <= 5) {
                ret += (int) (Math.random() * 40); //[0,39]
        }

        else if ( ((exam && randStudentSkill > 7) || (!exam && randStudentSkill > 5)) && randStudentSkill <= bounds[0]) {
            ret += ((int) (Math.random() * 10) + 40); // [40,49]
        } else if (randStudentSkill <= bounds[1]) {
            ret += ((int) (Math.random() * 20) + 50); // [50,69]
        } else if (randStudentSkill <= bounds[2]) {
            ret += ((int) (Math.random() * 20) + 70); // [70,89]
        } else {
            ret += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        ret += "\t";
        return ret;
    }

    /**
     * Read contents from file
     *
     * @param filename
     * @return
     */
    public List<String> readFile(String filename) {
        List<String> ret = new ArrayList<>();
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            while ((line = br.readLine()) != null) {
                ret.add(line);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return ret;
    }

    public void writeFile(String filename, String output){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            bw.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String formatStudentID(int id){
        String ret;
        if (id / 10 < 1) {
            ret = "000" + id;
        } else if (id / 100 < 1) {
            ret = "00" + id;
        } else if (id / 1000 < 1) {
            ret = "0" + id;
        } else {
            ret = String.valueOf(id);
        }
        ret += "\t";
        return ret;
    }

    public String generateStudentName(){
        int randFNIndex = (int) (Math.random() * firstNameList.size());
        int randSNIndex = (int) (Math.random() * surnameList.size());
        return  surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
    }

    public ExcelWorker() {
        firstNameList = readFile(fileFirstname);
        surnameList = readFile(fileSurname);
    }

    public void start(){
        StringBuffer outputBuffer = new StringBuffer();

        for (int i = 1; i <= CLASS_SIZE; i++) {
            // add '0' to fit 4 digits for student ID
            String student = "";
            student += formatStudentID(i);

            student += generateStudentName();

            //Student Skill
            int randStudentSkill = (int) (Math.random() * 101);

            // Labs
            for (int j = 0; j < NUM_LABS; j++) {
                student += generateScore(randStudentSkill, LAB_BOUNDS, false);
            }

            // Test
            student += generateScore(randStudentSkill, TEST_BOUNDS, false);

            // Exam
            student += generateScore(randStudentSkill, EXAM_BOUNDS, true);

            student += "\n";
            outputBuffer.append(student);
        }

        writeFile(fileOut, outputBuffer.toString());

    }


    public int[] getLAB_BOUNDS() {
        return LAB_BOUNDS;
    }

    public int[] getTEST_BOUNDS() {
        return TEST_BOUNDS;
    }

    public int[] getEXAM_BOUNDS() {
        return EXAM_BOUNDS;
    }
}

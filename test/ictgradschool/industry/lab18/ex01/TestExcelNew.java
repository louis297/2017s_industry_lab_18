package ictgradschool.industry.lab18.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Write tests
 */
public class TestExcelNew {
    ExcelWorker ew;

    @Before
    public void setUp(){
        ew = new ExcelWorker();
    }

    @Test
    public void TestGenerateScore(){
        int v = Integer.parseInt(ew.generateScore(3, ew.getLAB_BOUNDS(), false).trim());
        assertTrue(v>=0 && v<= 39);
        v = Integer.parseInt(ew.generateScore(10, ew.getLAB_BOUNDS(), false).trim());
        assertTrue(v>=40 && v<= 49);
        v = Integer.parseInt(ew.generateScore(20, ew.getLAB_BOUNDS(), false).trim());
        assertTrue(v>=50 && v<= 69);
        v = Integer.parseInt(ew.generateScore(40, ew.getLAB_BOUNDS(), false).trim());
        assertTrue(v>=70 && v<= 89);
        v = Integer.parseInt(ew.generateScore(80, ew.getLAB_BOUNDS(), false).trim());
        assertTrue(v>=90 && v<= 100);

        v = Integer.parseInt(ew.generateScore(3, ew.getTEST_BOUNDS(), false).trim());
        assertTrue(v>=0 && v<= 39);
        v = Integer.parseInt(ew.generateScore(10, ew.getTEST_BOUNDS(), false).trim());
        assertTrue(v>=40 && v<= 49);
        v = Integer.parseInt(ew.generateScore(40, ew.getTEST_BOUNDS(), false).trim());
        assertTrue(v>=50 && v<= 69);
        v = Integer.parseInt(ew.generateScore(80, ew.getTEST_BOUNDS(), false).trim());
        assertTrue(v>=70 && v<= 89);
        v = Integer.parseInt(ew.generateScore(98, ew.getTEST_BOUNDS(), false).trim());
        assertTrue(v>=90 && v<= 100);

        String s = ew.generateScore(3, ew.getEXAM_BOUNDS(), false).trim();
        v = s.isEmpty() ? -1 : Integer.parseInt(s);
        assertTrue(v>=0 && v<= 39 || s.isEmpty());
        v = Integer.parseInt(ew.generateScore(19, ew.getEXAM_BOUNDS(), false).trim());
        assertTrue(v>=40 && v<= 49);
        v = Integer.parseInt(ew.generateScore(59, ew.getEXAM_BOUNDS(), false).trim());
        assertTrue(v>=50 && v<= 69);
        v = Integer.parseInt(ew.generateScore(89, ew.getEXAM_BOUNDS(), false).trim());
        assertTrue(v>=70 && v<= 89);
        v = Integer.parseInt(ew.generateScore(91, ew.getEXAM_BOUNDS(), false).trim());
        assertTrue(v>=90 && v<= 100);
    }

    @Test
    public void TestFormatStudentId(){
        assertEquals("0001", ew.formatStudentID(1).trim());
        assertEquals("0010", ew.formatStudentID(10).trim());
        assertEquals("0100", ew.formatStudentID(100).trim());
        assertEquals("3456", ew.formatStudentID(3456).trim());
        assertEquals("1000", ew.formatStudentID(1000).trim());
    }
}